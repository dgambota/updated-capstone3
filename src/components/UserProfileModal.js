import {Modal, Container, Card, Col, Row, Form, Button, Tabs, Tab, Accordion} from 'react-bootstrap'
import {useState} from 'react'
import Swal2 from 'sweetalert2'
import UserProfileOrdersTab from '../components/UserProfileOrdersTab'

let handleShow = () => {}
export function showProfileModal(profileDetails){
	handleShow(profileDetails);
}

export default function UserProfileModal(){

	// Modal functions
	const [show, setShow] = useState(false);
    const handleClose = () => {
    	setShow(false)
    	setNewPass('');
    	setIsPassHidden(true);
    	setIsSavePass('Change Password');
    	setModalBackDrop('true');
		setModalKeyAccess(true);
    };
  	handleShow = (data) => {
  		setProfileAccEmail(data.email);
  		setProfileAccIsAdmin(data.isAdmin);
  		setProfileAccId(data._id);
  		setUserCart(data.cart.map(item => {
				return(
					<Row key={item._id}>
						<Col>{item.productId}</Col>
						<Col>{item.quantity}</Col>
						<Col>{item.subtotal}</Col>
					</Row>
				);
			}))
  		setUserOrders();
  		setShow(true);
  	}
  	const [modalBackDrop, setModalBackDrop] = useState('true');
  	const [modalKeyAccess, setModalKeyAccess] = useState(true);


  	const [newPass, setNewPass] = useState('');
  	const [isPassHidden, setIsPassHidden] = useState(true);
  	const [isSavePass, setIsSavePass] = useState('Change Password');


  	const [profileAccEmail, setProfileAccEmail] = useState('');
  	const [profileAccIsAdmin, setProfileAccIsAdmin] = useState('');
  	const [profileAccId, setProfileAccId] = useState('');


  	const [userCart, setUserCart] = useState([]);
  	const [userOrders, setUserOrders] = useState([]);



	// FUNCTION TO CHANGE PASS
	function changeUserPassword(userId){
		setModalBackDrop('static');
		setModalKeyAccess(false);
		if(isSavePass === 'Save'){
			fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/reset`,{
				method: 'PATCH',
				headers: {
					'Content-type' : 'application/json',
					'Authorization' : `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					password: newPass
				})
			})
			.then(response => response.json())
			.then(data => {
				if(data === true){
					handleClose();
					setIsPassHidden(true)
					setNewPass('');
					setIsSavePass('Change Password')
					Swal2.fire({
						title: 'Updated user password!',
						icon: 'success',
					})
				}
			})
			
		} else{
			setIsPassHidden(false)
			setIsSavePass('Save')
		}
	}

	// FUNCTION TO SET USER AS ADMIN
	function setUserAsAdmin(account){
		let isAdminVar;
		if(profileAccIsAdmin === true){
			isAdminVar = false;
		} else{
			isAdminVar = true;
		}

		fetch(`${process.env.REACT_APP_API_URL}/users/${account}/setAsAdmin`,{
			method: 'PUT',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: isAdminVar
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data === true){
				handleClose();
				Swal2.fire({
					title: 'Updated Admin Status!',
					icon: 'success',
					text: 'Set user to admin successfully.'
				})
			}
		})
	}

	return(
		<Modal size='xl' show={show} onHide={handleClose} backdrop={modalBackDrop} keyboard={modalKeyAccess} centered>
			<Modal.Header closeButton>
			  <Modal.Title>User Details</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Container>
				<Card>
					<Card.Body>
						<Row>
						      <Col xl={8}>
						        <Card.Title>Profile</Card.Title>
						      </Col>
						      <Col xl={3}>
						        <Form.Control type="text" placeholder='Type New Password' hidden={isPassHidden} value={newPass} onChange={(e) => setNewPass(e.target.value)}/>
						      </Col>
						</Row>
						<Row>
							<Col xl={4}>
								<Form.Label>Account Email</Form.Label>
							</Col>
							<Col xl={4}>
					        	<Form.Label>Admin</Form.Label>
					        </Col>
						</Row>
						<Row>
						      <Col xl={4}>
						        <Form.Control readOnly type="text" value={profileAccEmail}/>
						      </Col>
						      <Col xl={4}>
						        <Form.Control readOnly type="text" value={profileAccIsAdmin}/>
						      </Col>
						      <Col cx={2}>
						        <Button variant="success" onClick={e => changeUserPassword(profileAccId)}>{isSavePass}</Button>
						      </Col>
						      {
						      profileAccIsAdmin === false
						      ?
							      <Col xl={2}>
							        <Button variant="success" onClick={e => setUserAsAdmin(profileAccId)}>Set as Admin</Button>
							      </Col>
						      :
							      <Col xl={2}>
							        <Button variant="danger" onClick={e => setUserAsAdmin(profileAccId)}>Remove as Admin</Button>
							      </Col>
						  	  }
						</Row>
						<Row>
							<Tabs defaultActiveKey="cart" id="tabNav" className="mb-3 mt-3">
						     <Tab eventKey="cart" title="Cart">
								{userCart}
						     </Tab>
						     <Tab eventKey="orderhistory" title="Order History">
								<UserProfileOrdersTab id={profileAccId}/>
						     </Tab>
						   </Tabs>
						</Row>
					</Card.Body>
				</Card>	
				</Container>
	        </Modal.Body>
			<Modal.Footer>
				<Button variant="outline-danger" onClick={handleClose}>
				Close
				</Button>
			</Modal.Footer>
		</Modal>
	)
}