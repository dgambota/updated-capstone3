/* IMPORT REACT MODULES */
import {Toast, ToastContainer, Button} from 'react-bootstrap'
import {useEffect, useState} from 'react';

let addToast;

/* FUNCTION TO ADD AND DISPLAY TOAST */
export function showToast(toastDetails){
	addToast(toastDetails)
}

export default function ToastNotification(){
	
	const [toastList, setToastList] = useState([]);
	let toastProp = {};

	/* FUNCTION TO PASS TOAST DETAILS AND ADD TO LIST */
	addToast = (toastDetails) => {
		toastProp = {
			id: 1,
			title: toastDetails.title,
			description: toastDetails.description,
			bgcolor: toastDetails.bgcolor
		}
		setToastList([...toastList, toastProp])
	}

	 return (
      <ToastContainer variant='dark' position="top-end" className="p-3" style={{ zIndex: 1,  position: 'fixed', marginTop: '5rem', color: 'white'}}>
			{
        toastList.map((toast, i) => (
			<Toast key={i} id={`toast${i}`} bg={toast.bgcolor} delay={3000} autohide onClose={(e) => document.querySelector(`#toast${i}`).classList.remove('show')}>
				<Toast.Header>
					<strong className="me-auto">{toast.title}</strong>
					<small className="text-muted"></small>
				</Toast.Header>
				<Toast.Body>{toast.description}</Toast.Body>
			</Toast>
        ))
			}
      </ToastContainer>
  );
}