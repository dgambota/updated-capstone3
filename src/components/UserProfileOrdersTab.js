import {Accordion} from 'react-bootstrap'
import {useEffect, useState} from 'react'

export default function UserProfileOrdersTab(account){

	const [userOrders, setUserOrders] = useState([]);

	// Function to access route to display orders made by user selected and display to accordion
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/${account.id}/orders`,{
			method: 'POST',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			let count = 0;
			setUserOrders(data.map(account => {
				let prod = [];

				account.products.map(item => {
					prod.push(<Accordion.Body key={item._id}>{item._id}</Accordion.Body>);
				})

				count++;
				return(
					<Accordion.Item eventKey={account._id} key={account._id}>
					  <Accordion.Header>Order #{count}</Accordion.Header>
					  {prod}
					</Accordion.Item>
				)
			}));
		})
	}, [])

	return(
		<Accordion>
			{userOrders}
		</Accordion>
	)
}