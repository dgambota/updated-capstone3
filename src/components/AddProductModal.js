/* IMPORT REACT MODULES */
import {useState, useEffect} from 'react'
import {Modal, Container, Row, Col, Button, Form} from 'react-bootstrap'

/* IMPORT OTHER REQUIRED MODULE*/
import Swal2 from 'sweetalert2'
import {saveUploadImg, getImgID} from '../components/UploadImageController'

/* FUNCTION TO CHANGE MODAL STATE */
let showAddProdModal = () => {}
export function ShowAddModal(){
	showAddProdModal()
}

export default function AddProductModal(){

	

	/* MODAL FUNCTIONS */
  	const [showAddModal, setShowAddModal] = useState(false);
    const closeAddProdModal = () => setShowAddModal(false);
  	showAddProdModal = () => setShowAddModal(true);

	/* ADD PRODUCT FIELDS STATE */
	const [imgIdHolder, setImgIdHolder] = useState('')
	const [productName, setProductName] = useState('');
	const [productDescription, setProductDescription] = useState('');
	const [productPrice, setProductPrice] = useState('');
	const [productQuantity, setProductQuantity] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	/* HOOK FOR FIELDS TO DISABLE ADD BUTTON IF EMPTY */
	useEffect(() => {
		if(productName != '' && productDescription != '' && productPrice != 0 && productQuantity != 0){
			return setIsDisabled(false);
		}
		return setIsDisabled(true);
	}, [productName, productDescription, productPrice, productQuantity])
	/* END */


	function saveImg(e, file, holder){
		saveUploadImg(e, file, holder)
	}

	/* FUNCTION TO ADD PRODUCT TO DATABASE */
	function addProduct(e){
		e.preventDefault();
		console.log(getImgID())
		fetch(`${process.env.REACT_APP_API_URL}/products` , {
			method: 'POST',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				imgId: getImgID(),
				name : productName,
				description : productDescription,
				price : productPrice,
				quantity : productQuantity
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data === true){
				Swal2.fire({
					title: 'Successfully Added Product!',
					icon: 'success',
					text: 'Check your products.'
				})
				closeAddProdModal();
				setProductName('');
				setProductDescription('');
				setProductPrice('');
				setProductQuantity('');
			}
		})
	}
	/* END ADD PRODUCT FUNCTION */

	return(
		<Modal size='xl' show={showAddModal} onHide={closeAddProdModal} backdrop="static" keyboard={false} centered>
			<Modal.Header closeButton>
			  <Modal.Title>Add Product</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Container>
				<Form onSubmit={e => addProduct(e)}>
					<Row>
						<Col xl={1} />
						<Col xl={4}>
							<Form.Group className="mb-3">
						        <Form.Control id="imgFile" type="file" onChange={(e) => saveImg(e, 'imgFile', 'imgHolder')}/>
						    </Form.Group>
						    <img id="imgHolder" />
						</Col>
						<Col xl={2} />
						<Col xl={4}>
							<Row>
								<Col xl={12}>
									<Row>
								      <Form.Group className="mb-3" controlId="productName">
								      <Form.Text>Product Name</Form.Text>
								        <Form.Control type="text" placeholder="Enter Product Name" value={productName} onChange={(e) => setProductName(e.target.value)}/>
								      </Form.Group>
							      	</Row>
							      	<Row>
								      <Form.Group className="mb-3" controlId="productDescription">
								      <Form.Text>Description</Form.Text>
								        <Form.Control as="textarea" rows={5} placeholder="Enter Description" value={productDescription} onChange={(e) => setProductDescription(e.target.value)}/>
								      </Form.Group>
								    </Row>
								    <Row>
								    	<Col xl={6}>
								    	<Form.Group className="mb-3" controlId="productPrice">
								    	<Form.Text>Price</Form.Text>
								    	  <Form.Control type="text" placeholder="Price" value={productPrice} onChange={e => setProductPrice(e.target.value)}/>
								    	</Form.Group>
								    	</Col>
								    	<Col xl={6}>
								    	<Form.Group className="mb-3" controlId="productQuantity">
								    	  <Form.Text>Quantity</Form.Text>
								    	  <Form.Control type="text" placeholder="Quantity" value={productQuantity} onChange={e => setProductQuantity(e.target.value)}/>
								    	</Form.Group>
								    	</Col>
								    </Row>
					      		</Col>
							</Row>
							<Row className="pt-5">
								<Col xl={7} />
								<Col xl={5}>
									<Button variant="success" type="submit" disabled={isDisabled}>
									  Add Product
									</Button>
								</Col>
							</Row>
						</Col>
				      	<Col xl={1} />
					</Row>
					
				</Form>
				</Container>
	        </Modal.Body>
			<Modal.Footer>
				<Button variant="outline-danger" onClick={closeAddProdModal}>
				Discard Changes
				</Button>
			</Modal.Footer>
		</Modal>
	)
}