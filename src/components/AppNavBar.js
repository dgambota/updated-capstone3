/* REACT MODULES */
import {Container, Navbar, Nav} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {NavLink, Link} from 'react-router-dom';

/* MANUAL DEFINED STYLES */
import style from '../stylesheets/NavbarCSS.module.css'

import UserContext from '../UserContext';


export default function AppNavBar(){

    const {user, currentPage} = useContext(UserContext);
    const logoWhite = `https://res.cloudinary.com/dilv1kuce/image/upload/v1689736748/logo-white-no-background_vlmhgd.png`;
    const logoBrown = `https://res.cloudinary.com/dilv1kuce/image/upload/v1689736749/logo-brown-no-background_tm2ndk.png`
    /* VARIABLES FOR CHANGING NAVBAR STYLE AFTER SCROLLED */
    let navHeight = 0;
    const [navScrollClass, setNavScrollClass] = useState({navClass: style.navContainerStart, navLogo: logoBrown});



    useEffect(() => {
        if(currentPage == 'login'){
            setNavScrollClass({
                navClass: style.navContainerTransparent,
                navLogo: logoWhite,
                bgcolor: 'light'
            })
        }

        if(currentPage == 'register'){
            setNavScrollClass({
                navClass: style.navContainerTransparent,
                navLogo: logoBrown,
                bgcolor: 'dark'
            })
        } 

        if(currentPage == 'home' || currentPage == 'products'){
             setNavScrollClass({
                navClass: style.navContainerStart,
                navLogo: logoBrown,
                bgcolor: 'light'
            })
        }
    }, [currentPage])


	return (
		<Navbar fixed='top' expand="lg" className={navScrollClass.navClass} variant={navScrollClass.bgcolor}>
            <Container fluid>
                <Navbar.Brand as = {Link} to = '/'><img src={navScrollClass.navLogo} className={style.logo} /></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                    <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>

                    {
                        user.id === null || user.id === undefined
                        ?
                        <>
                            <Nav.Link className={style.navItem} as = {NavLink} to = '/login'>Login</Nav.Link>
                            <Nav.Link className={style.navItem} as = {NavLink} to = '/register'>Register</Nav.Link>
                            <Nav.Link className={style.navItem} as = {NavLink} to = '/products'>Products</Nav.Link>
                        </>
                        :
                        user.isAdmin === true 
                        ?
                        <>
                            <Nav.Link as = {NavLink} to = '/adminDashboard'>Admin Dashboard</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/users'>User Management</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/orders'>Orders</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
                        </>
                        :
                        <>
                            <Nav.Link as = {NavLink} to = '/products'>Products</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/cart'>Cart</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/orders'>Orders</Nav.Link>
                            <Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
                        </>
                    }   

                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
	)
}