import {useState, useEffect} from 'react'
import axios from 'axios'
import {Col, Pagination} from 'react-bootstrap'

let paginate = () => {}
export function paginateComponent(objects){
	paginate(objects);
}

export default function PaginationHandler(){

	const [posts, setPosts] = useState([]);
	const [loading, setLoading] = useState(false);
	const [currentPage, setCurrentPage] = useState(1);
	const [postsPerPage, setPostsPerPage] = useState(10);


	const indexOfLastPost = currentPage * postsPerPage;
	const indexOfFirstPost = indexOfLastPost - postsPerPage
	const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost)


	paginate = (objects) => {
		setPosts(objects);
	}

	
	let items = [];
	for (let number = 1; number <= Math.ceil(posts.length / postsPerPage); number++) {
	  items.push(
	    <Pagination.Item key={number} active={number == currentPage} onClick={(e) => setCurrentPage(e.target.innerHTML)}>
	      {number}
	    </Pagination.Item>,
	  );
	}




	return(
		<Col className="mt-5">
			<div>
			    <Pagination>
			    	{items}
			    </Pagination>
			  </div>
		</Col>
	)
}