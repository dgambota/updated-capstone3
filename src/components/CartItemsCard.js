import {Col, Card, Row, Form, Button} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import style from '../stylesheets/Cart.module.css'
import Swal2 from 'sweetalert2';
import {showToast} from '../components/ToastNotification'
import {refreshCart} from '../pages/Cart'

let localTotal = 0;

export default function CartItemsCard(item){
	
	const [total, setTotal] = useState(0);
	const [productDetails, setProductDetails] = useState([]);
	const [productQuantity, setProductQuantity] = useState(0);
	const [itemSubTotal, setItemSubTotal] = useState(0);

	const [deductBtn, setDeductBtn] = useState(false);
	const [addBtn, setAddBtn] = useState(false);
	const [qtyField, setQtyField] = useState(false);


	function getProductDetails(productId){
		fetch(`${process.env.REACT_APP_API_URL}/products/${item.details.productId}`)
		.then(response => response.json())
		.then(data => {
			setProductDetails(data)
		})
	}

	useEffect(() => {
		setItemSubTotal(item.details.subtotal)
		setProductQuantity(item.details.quantity)
		getProductDetails(item.details.productId);
	}, [])

	


	/* FUNCTION TO REMOVE ITEM FROM CART ARRAY */
	function removeFromCart(prodId){
		fetch(`${process.env.REACT_APP_API_URL}/cart`, {
			method: 'DELETE',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: prodId
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data.status === 'success'){
				showToast({
					title: 'Item Removed from Cart',
					description: 'You cart has been updated',
					bgcolor: 'success'
				})
			}
			if(data.status === 'empty'){
				Swal2.fire({
					title: 'Your Cart is Empty!',
					icon: 'error',
					text: 'Check our products and add one to cart.'
				})
			}

		})
	}	
	/* END FUNCTION REMOVE FROM CART */

	/* FUNCTION TO CHECK QUANTITY AND UPDATE DATABASE */
	function updateQuantity(prodId, method){
		let quantity = productQuantity;
		quantity < 2 && method == '-' ? 
		showToast({
			title: 'Cannot deduct more item',
			description: 'If you wish to remove item, click remove',
			bgcolor: 'danger'
		})
		:
 		method == '+' ? quantity += 1 : quantity -= 1
		setProductQuantity(quantity)
		setQtyField(true);
		setAddBtn(true)
		setDeductBtn(true)

		fetch(`${process.env.REACT_APP_API_URL}/cart`, {
			method: 'PUT',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: prodId,
				quantity: quantity,
				price: productDetails.price
			})
		})
		.then(response => response.json())
		.then(data => {
			refreshCart().then(() => {
				setItemSubTotal(data.subtotal);
				setQtyField(false);
				setAddBtn(false)
				setDeductBtn(false)
			})
		})
	}
	/* END FUNCTION CHECK QUANTITY */

	return (
		<Col xs={12} md={12} xl={12} >
		    <Card className={style.productCard} key = {item.details._id} className="mb-3">
		    	<Row>
		    		<Col xl={3} >
		    			<Card.Img className={style.productCard} variant='top' src={`https://res.cloudinary.com/dilv1kuce/image/upload/c_fill/${productDetails.imgId}.jpg`}/>
		    		</Col>
			        <Col xl={4}>
			        	<Row className="mb-3 mt-3">
			        	<Card.Title>
			        	    <h5>{productDetails.name}</h5>
			        	</Card.Title>
			        	</Row>
			        	<Row>
			        		<p>{productDetails.description}</p>
			        	</Row>
			        </Col>
			        <Col xl={3}>
			        	<Row className="mb-5 mt-3">
			        		<Col xl={12} className="mx-auto">
			        			<p>{productDetails.price}</p>
			        		</Col>
			        	</Row>
			        	<Row>
			        	<Col xl={2}>
		        	    	<Button disabled={deductBtn} variant='success' onClick={() => updateQuantity(item.details.productId, '-')}>-</Button>
		        	    </Col>
			        	<Col xl={7}>
			        		<Form.Control disabled={qtyField} readOnly type="text" value={productQuantity}/>
			        	</Col>
			        	<Col xl={2}>
		        	    	<Button disabled={addBtn} variant='success' onClick={() => updateQuantity(item.details.productId, '+')}>+</Button>
		        	    </Col>
		        	    </Row>
		            </Col>
		            <Col xl={2}>
		            	<Row className="mb-5 mt-3">
		            		<Col xl={12}>
		            		<h5 type="text" id={`subtotal${item.details.productId}`}>{itemSubTotal}</h5>
		            		</Col>
		            	</Row>
		            	<Row>
		            		<Col xl={12}>
		            		<Button variant='outline-danger' onClick={e => removeFromCart(item.details.productId)}>Remove</Button>
		            		</Col>
		            	</Row>
		            </Col>

			        
		        </Row>
		    </Card>
		</Col>
	)
}