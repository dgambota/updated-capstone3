/* IMPORT REACT MODULES */
import {Col, Card, Placeholder} from 'react-bootstrap'
import {useState, useEffect} from 'react'

/* IMPORT REQUIRED STYLESHEETS */
import style from '../stylesheets/ProductsCatalogCSS.module.css'
const logo = 'https://res.cloudinary.com/dilv1kuce/image/upload/v1689736748/logo-white-no-background_vlmhgd.png'

export default function LoadingPlaceholder(loadType){

	const [loadedPlaceholder, setLoadedPlaceholder] = useState([]);

	/* FUNCTION TO FILL CATALOG WITH TEMPORARY LOADING PLACEHOLDER */
	function productsPlaceholder(){
		let load = [];
		for(let i = 0; i < 20; i++){
			load.push(
				<Col key={i} xl={2} md={3} xs={6} sm={4}>
			        <Placeholder as={Card} animation="glow" className={style.colHeight} >
			        	<Placeholder as={Card} >
			        		<div as={Placeholder} className={style.cardImg}>
			        		<Card.Img animation="glow" src={logo} style={{marginTop: '5rem', minHeight: '4rem', maxHeight: '4rem', objectFit: 'fill'}}/>
			        		</div>
			        	</Placeholder>
			        </Placeholder>
				</Col>
			)
		}
		setLoadedPlaceholder(load);
	}
	/* END FUNCTION FILL LOADING PLACEHOLDER */


	/* FUNCTION TO CHECK TYPE OF CONTENT WHEN CALLED */
	useEffect(() => {
		if(loadType.loadPlaceholder == 'products'){
			productsPlaceholder();
		}
		
	}, [])
	/* END */


	return(
		<>
			{loadedPlaceholder}
		</>
	)
}