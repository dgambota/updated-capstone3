/* REACT MODULES */
import { Button, Row, Col, Card, Placeholder } from 'react-bootstrap';	
import { useState, useEffect } from 'react';
import {Link} from 'react-router-dom';

/* OTHER REQUIRED MODULES */
import Swal2 from 'sweetalert2'
import PropTypes from 'prop-types';
import style from '../stylesheets/ProductsCatalogCSS.module.css'
import {showToast} from '../components/ToastNotification'




export default function ProductsCard({productsProp}){

    const {imgId, _id, name, description, price, quantity} = productsProp;

    const [isDisabled, setIsDisabled] = useState();

    const [cart, setCart] = useState([]);
    
    /* FUNCTION TO ADD PRODUCT TO USER CART ARRAY */
    function addToCart() {
        fetch(`${process.env.REACT_APP_API_URL}/cart`, {
            method: 'PATCH',
            headers: {
                'Content-type' : 'application/json',
                'Authorization' : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                cart: [{
                    productId: _id,
                    quantity: 1
                    }]
            })
        })
        .then(response => response.json())
        .then(data => {
            if(data == true){
                showToast({
                    title: 'Success',
                    description: 'Item Added to Cart',
                    bgcolor: 'success'
                })
            }
        })
    }



	return (
		<Card className={style.colHeight} >
            <Card.Img variant='top' src={`https://res.cloudinary.com/dilv1kuce/image/upload/c_fill/${imgId}.jpg`} className={style.cardImg}/>
            <Card.ImgOverlay className={style.cardOverlay} >
                <div className={style.overlayContent} style={{minHeight: '16rem'}}>
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text></Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    <Card.Subtitle>Stock</Card.Subtitle>
                    <Card.Text>{quantity}</Card.Text>
                    <Button as = {Link} to = {`/products/${_id}`} variant="outline-light" disabled={isDisabled}>Check Details</Button>
                    <Button variant="outline-light" disabled={isDisabled} onClick={e => addToCart(_id)}>Add</Button>
                </div>
            </Card.ImgOverlay>
            
        </Card>
	)
}


ProductsCard.propTypes = {
    productsProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}