import {Row} from 'react-bootstrap'
import axios from 'axios'

let imgID = '';
let uploadImage = () => {}
export function saveUploadImg(e, inputId, imgHolder){
	uploadImage(e, inputId, imgHolder);
}

export function getImgID(){
	return imgID;
}

export default function UploadImageController(){
	
	  	uploadImage = async (e, inputId, imgHolder) => {
		e.preventDefault()
		const signatureResponse = await axios.get(`${process.env.REACT_APP_API_URL}/upload/get-signature`)

		const data = new FormData()
		data.append("file", document.querySelector(`#${inputId}`).files[0])
		data.append("api_key", 277875156272986)
		
		data.append("signature", signatureResponse.data.signature)
		data.append("timestamp", signatureResponse.data.timestamp)

		const cloudinaryResponse = await axios.post(`https://api.cloudinary.com/v1_1/dilv1kuce/image/upload`, data, {headers: { "Content-Type": "multipart/form-data" },
			onUploadProgress: function (e) {
			  console.log(e.loaded / e.total)
			}
		})
		document.querySelector(`#${imgHolder}`).src = `https://res.cloudinary.com/dilv1kuce/image/upload/w_400,h_500,c_fill,q_100/${cloudinaryResponse.data.public_id}.jpg`;

		imgID = cloudinaryResponse.data.public_id;

		// // send the image info back to our server
		// const photoData = {
		// 	public_id: cloudinaryResponse.data.public_id,
		// 	version: cloudinaryResponse.data.version,
		// 	signature: cloudinaryResponse.data.signature
		// }

		// axios.post(`${process.env.REACT_APP_API_URL}/do-something-with-photo`, photoData)
	}
}