import { Button, Row, Col, Card } from 'react-bootstrap';	
import {Link} from 'react-router-dom';
import '../App.css';

export default function Banner(){
	return (
		<Row>
            <Col xs={12} md={12} xl={12} >
                <Card className="cardHighlight">
                    <Card.Img variant='top' src={`https://res.cloudinary.com/dilv1kuce/image/upload/v1689736813/banner_ctpdhw.avif`} className='banner' />
                    <Card.ImgOverlay>
                        <Card.Title>
                            <h2></h2>
                        </Card.Title>
                        <Card.Text>
                            
                        </Card.Text>
                    </Card.ImgOverlay>
                </Card>
            </Col>
        </Row>
	)
}