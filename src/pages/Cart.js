/* IMPORT REACT MODULES */
import UserContext from '../UserContext';
import {useContext, useEffect, useState} from 'react'
import {useNavigate} from 'react-router-dom'
import {Container, Row, Col, Table, Button, Form, Card} from 'react-bootstrap'

/* IMPORT OTHER REQUIRED COMPONENTS */
import PageNotFound from '../pages/PageNotFound';
import CartItemsCard from '../components/CartItemsCard'
import Swal2 from 'sweetalert2';
import style from '../stylesheets/Cart.module.css'

let getCart = () =>{}
export function refreshCart(){
	return getCart();
}

export default function Cart(){

	const navigate = useNavigate();
	const {user} = useContext(UserContext);
	const {currentPage, setCurrentPage} = useContext(UserContext)

	const [updatedQuantity, setUpdatedQuantity] = useState(0);
	const [cart, setCart] = useState([]);
	const [total, setTotal] = useState(0);

	/* FUNCTION TO GET TOTAL AND UPDATE STATE */
	function getTotal(){

		fetch(`${process.env.REACT_APP_API_URL}/cart`, {
			method: 'POST',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			let totalAmount = 0;
			if(data.length > 0){
				data.map(item => {
					totalAmount += item.subtotal;
					setTotal(totalAmount);
				})
			}
		})
	}
	/* END FUNCTION GET TOTAL */

	/* FUNCTION TO FILL CART WITH PRODUCTS ADDED */
	getCart = async () => {
		await fetch(`${process.env.REACT_APP_API_URL}/cart`, {
			method: 'POST',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			if(data.length > 0){
				setCart(data.map(item => {
					return(
						<CartItemsCard key={item._id} details={item}/>
					)
				}))
				getTotal();
			} else{
				setCart(
					<tr key = {0}>
						<td colSpan={4}><h1>Your cart is Empty</h1></td>
					</tr>
				)
			}
		})
	}
	/* END FUNCTION FILL CART */

	useEffect(() => {
		getCart();
	}, [cart])

	
	/* FUNCTION TO PASS AND EMPTY CART DETAILS AND CREATE NEW ORDER */
	function checkout(){
		fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
			method: 'POST',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			if(data === true){
				Swal2.fire({
					title: 'Checkout Successfull!',
					icon: 'success',
					text: 'Your products will be delivered soon.'
				})
				navigate('/orders');
			}
			if(data.status === 'empty'){
				Swal2.fire({
					title: 'Your Cart is Empty!',
					icon: 'error',
					text: 'Check our products and add one to cart.'
				})
			}
		})
	}
	/* END FUNCTION CHECKOUT */


	

	return(
		user.isAdmin === false 
		?
		<Container>
			<Row style={{marginTop: '5rem'}}>
				<Col xl={8} className="mt-5">
					<h2>Items</h2>
					{cart}
				</Col>
				<Col xl={4} style={{marginTop: '6rem'}}>
					<Card className="cardHighlight">
					<Card.Title className="mx-auto mt-2">
						<Row>
							<Col xl={12}>
				            	<h2>Order Details</h2>
					        </Col>
					    </Row>
				    </Card.Title>
				    <Card.Body>
				    <Row className="mx-auto py-1">
				    	<Col xl={6}>
				    		Subtotal:
				    	</Col>
				    	<Col xl={6}>
				    		{total}
				    	</Col>
				    </Row>
				    <Row className="mx-auto py-1">
				    	<Col xl={6}>
				    		Shipping:
				    	</Col>
				    	<Col xl={6}>
				    		$$$
				    	</Col>
				    </Row>
				    <Row className="mx-auto py-1">
				    	<Col xl={6}>
				    		Taxes:
				    	</Col>
				    	<Col xl={6}>
				    		$$$
				    	</Col>
				    </Row>
				    <Row className="mx-auto py-5">
			        	<Col xl={6}>
			        		<h5>Total:</h5>
			        	</Col>
			        	<Col xl={6}>
			        		<h5></h5>
			        	</Col>
				    </Row>
				    <Row>
				    	<Button variant='success' onClick={e => checkout()}>Checkout</Button>
				    </Row>
				    </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		:
		<PageNotFound />
	)
}