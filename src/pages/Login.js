/* IMPORT REACT MODULES */
import {Form, Button, Col, Row} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useNavigate} from 'react-router-dom';

/* IMPORT OTHER REQUIRED COMPONENTS */
import PageNotFound from '../pages/PageNotFound';
import Swal2 from 'sweetalert2';
import UserContext from '../UserContext';

/* IMAGES AND STYLESHEET - FOR PRESENTATION */
import style from '../stylesheets/Login.module.css'
const bg1 = 'https://res.cloudinary.com/dilv1kuce/image/upload/v1689736791/Blue-Wood-Background_xddfxc.jpg'
const bg2 = 'https://res.cloudinary.com/dilv1kuce/image/upload/v1689736792/Multicolor-Wood-Background_jgleyt.jpg'
const bg3 = 'https://res.cloudinary.com/dilv1kuce/image/upload/v1689736787/Horse-Design-Wood-Background_kn46ts.jpg'
const bg4 = 'https://res.cloudinary.com/dilv1kuce/image/upload/v1689736794/Log-Wood-Background_bipshq.jpg'
const bg5 = 'https://res.cloudinary.com/dilv1kuce/image/upload/v1689736788/Wood-Background-Wallpaper_mwxvpw.jpg'
const bg6 = 'https://res.cloudinary.com/dilv1kuce/image/upload/v1689736786/Colorful-Wood-Background_oetfos.jpg'

export default function Login(){

	const navigate = useNavigate();


	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	const {user, setUser} = useContext(UserContext);
	const {currentPage, setCurrentPage} = useContext(UserContext);
	// console.log(currentPage)
	setCurrentPage('login')

	/* HOOK TO CHECK IF EMAIL AND PASSWORD FIELD IS EMPTY */
	useEffect(() => {
		if(email != '' && password != ''){
			return setIsDisabled(false);
		}
		return setIsDisabled(true);
	}, [email, password])
	/* END FUNCTION CHECK EMAIL & PASS FIELD */

	/* FUNCTION TO AUTHENTICATE USER LOGIN DETAILS */
	function login(e){
		e.preventDefault();


		fetch(`${process.env.REACT_APP_API_URL}/users/login` , {
			method: 'POST',
			headers: {
				'Content-type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data === false){
				Swal2.fire({
					title: 'Login unsuccessful',
					icon: 'error',
					text: 'Check your login credentials and try again'
				})
			} else {
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal2.fire({
					title: 'Login successful',
					icon: 'success',
					text: 'Enjoy shopping with us!'
				})

				navigate('/');
			}
		})
	}
	/* END FUNCTION AUTHENTICATE */

	/* FUNCTION TO RETRIEVE USER DETAILS AND PASS TO USERCONTEXT */
	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}
	/* END FUNCTION */

	return(
		user.id === null || user.id === undefined
		?
		<Row>
			<Col xl={6}>
				<Row>
					<Col xl={4} className={style.noOverflow}>
						<div><img className={style.sideImg} src={bg1}/></div>
					</Col>
					<Col xl={4} className={style.noOverflow}>
						<div><img className={style.sideImg} src={bg2}/></div>
					</Col>
					<Col xl={4} className={style.noOverflow}>
						<div><img className={style.sideImg} src={bg3}/></div>
					</Col>
				</Row>
				<Row>
					<Col xl={4} className={style.noOverflow}>
						<div><img className={style.sideImg} src={bg4}/></div>
					</Col>
					<Col xl={4} className={style.noOverflow}>
						<div><img className={style.sideImg} src={bg5}/></div>
					</Col>
					<Col xl={4} className={style.noOverflow}>
						<div><img className={style.sideImg} src={bg6}/></div>
					</Col>
				</Row>
			</Col>
			<Col className="mx-auto mt-5" xl={6}>
				<Row className="mt-5">
					<Col xl={2} className="mx-auto">
					<h3>Login</h3>
					</Col>
				</Row>

				<Form>
				<Row className="mt-5">
			      <Form.Group className="mb-3" controlId="formBasicEmail">
			      <Col xl={6} className="mx-auto">
			        <Form.Label className={style.label}>Email address</Form.Label>
			      </Col>
			      <Col xl={6} className="mx-auto">
			        <Form.Control className={style.inputBox} type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
			      </Col>
			      </Form.Group>
			    </Row>
			    <Row>
			      <Form.Group className="mb-3" controlId="formBasicPassword1">
			      <Col xl={6} className="mx-auto">
			        <Form.Label className={style.label}>Password</Form.Label>
		       	  </Col>
		       	  <Col xl={6} className="mx-auto">
			        <Form.Control className={style.inputBox} type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
			      </Col>
			      </Form.Group>
			    </Row>
			    <Row>
			    <Col xl={2} className="mx-auto">
			      <Button variant="primary" onClick={(e) => login(e)} disabled={isDisabled}>
			        Login
			      </Button>
			    </Col>
			    </Row>
			    </Form>	
			</Col>
		</Row>
		:
		<PageNotFound />
	)
}