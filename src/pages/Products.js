/* IMPORT REACT MODULES */
import { Fragment, useEffect, useState, useContext, Link } from 'react';
import { Col, Row, Container, Form, Button, Card, Placeholder, Pagination } from 'react-bootstrap'

/* IMPORT MANUAL DEFINED COMPONENTS */
import ProductsCard from '../components/ProductsCard';
import UserContext from '../UserContext';
import PageNotFound from '../pages/PageNotFound';
import LoadingPlaceholder from '../components/LoadingPlaceholder';
import PaginationHandler, {paginateComponent} from '../components/Pagination'

/* IMPORT STYLESHEET */
import style from '../stylesheets/ProductsCatalogCSS.module.css'


export default function Products(){

	const {user, setUser} = useContext(UserContext);
	const {currentPage, setCurrentPage} = useContext(UserContext);

	const [products, setProducts] = useState([])
	const [searchInput, setSearchInput] = useState('');
	const [searchIsEmpty, setSearchIsEmpty] = useState(true);
	const [isProductsLoading, setIsProductsLoading] = useState(true);

	setCurrentPage('products');

	/* HOOK TO SEARCH PRODUCTS */
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`, {
			method: 'POST',
			headers: {
				'Content-type' : 'application/json'
			},
			body: JSON.stringify({
				name: searchInput
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data.length != 0){
				setProducts(data.map(products => {
					return(
						<Col key = {products._id} xl={2} md={3} xs={6} sm={4}>
							<ProductsCard  productsProp = {products} />
						</Col>
					)
				}));
				setSearchIsEmpty(false)
			} else{
				setSearchIsEmpty(true)
			}
		})
	}, [searchInput])
	/* END HOOK SEARCH PRODUCTS */


	/* HOOK GET ACTIVE PRODUCTS */
	useEffect(() => {
		if(searchIsEmpty == true){
			fetch(`${process.env.REACT_APP_API_URL}/products/active`)
			.then(response => response.json())
			.then(data => {
				setIsProductsLoading(false);
				setProducts(data.map(products => {
					return(
						<Col key = {products._id} xl={2} md={3} xs={6} sm={4}>
							<ProductsCard  productsProp = {products} />
						</Col>
					)
				}));
			})
		}
	}, [searchIsEmpty])	
	/* END HOOK GET ACTIVE PRODUCTS */



	const [loading, setLoading] = useState(false);
	const [currentPageSelected, setCurrentPageSelected] = useState(1);
	const [postsPerPage, setPostsPerPage] = useState(12);


	const indexOfLastPost = currentPageSelected * postsPerPage;
	const indexOfFirstPost = indexOfLastPost - postsPerPage
	const currentPosts = products.slice(indexOfFirstPost, indexOfLastPost)
	let items = [];

	function paginationHandler(){
		
		for (let number = 1; number <= Math.ceil(products.length / postsPerPage); number++) {
		  items.push(
		    <Pagination.Item key={number} active={number == currentPageSelected} onClick={(e) => setCurrentPageSelected(e.target.innerHTML)}>
		      {number}
		    </Pagination.Item>,
		  );
		}

	}

	
	paginationHandler();
	


	return(
		user.isAdmin === true
		?
		<PageNotFound />
		:
		<Container fluid style={{marginTop: '6rem'}}>
			<Form>
				<Row>
					<Col xl={6}>
						<Form.Group className="mb-3" controlId="productName">
						  <Form.Control type="text" placeholder="Enter Product Name" value={searchInput} onChange={e => setSearchInput(e.target.value)}/>
						</Form.Group>
					</Col>
				</Row>
			</Form>
			{
			isProductsLoading ?
			<Row className='gx-1'>
				<LoadingPlaceholder loadPlaceholder={'products'} />
			</Row>
			:
			<Row className='gx-1'>
				<Col>
					<Row>
					{currentPosts}
					</Row>
				<Row >
					<Col className="mt-5" >
						<div>
						    <Pagination >
						    	{items}
						    </Pagination>
						  </div>
					</Col>
				</Row>
				</Col>
			</Row>
			}
		</Container>
	)
}