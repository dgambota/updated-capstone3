import UserContext from '../UserContext'
import {useContext, useEffect, useState} from 'react';
import {Container, Row, Col, Table, Accordion, Toast, Button, Card, Carousel} from 'react-bootstrap'
import moment from 'moment';

import PageNotFound from '../pages/PageNotFound'
import style from '../stylesheets/Cart.module.css'

export default function OrderHistory(){
	const {user} = useContext(UserContext);

	const [userOrders, setUserOrders] = useState([]);



	/* FUNCTION TO FILL CONTAINER WITH USER ORDERS */
	useEffect(() => {
		let method = '', route = '';

		if(user.isAdmin === false)
		{
			route = `/users/${user.id}/orders`;
			method = 'POST';
		} else {
			route = '/orders/all';
			method = 'GET';
		}

		fetch(`${process.env.REACT_APP_API_URL}${route}`,{
			method: method,
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			let count = 0;
			setUserOrders(data.map(account => {
				let prod = [];
				
				account.products.map(item => {
					prod.push(
						<tr key={item._id}>
							<td>{item._id}</td>
							<td>{item.quantity}</td>
						</tr>
					);
				})

				count++;
				return(
					<Row>
					<Col xl={2}/>
					<Col xl={8}>

					<Accordion>
					<Accordion.Item eventKey={account._id} key={account._id}>
					{user.isAdmin ? 
					  <Accordion.Header>Order #{count} Created by User: {account.userId} Total:{account.totalAmount} Ordered On: {moment(account.purchasedOn).format('MMM DD, YYYY - h:mm:ss a - UTC Z')}</Accordion.Header>
					  :
					  <Accordion.Header>Order #{count} Total:{account.totalAmount} Ordered On: {moment(account.purchasedOn).format('MMM DD, YYYY - h:mm:ss a - UTC Z')}</Accordion.Header>
					}
					  <Accordion.Body >
					  	<Table>
							<thead>
								<th>Product Id</th>
								<th>Quantity</th>
							</thead>
							<tbody>
					  			{prod}
					  		</tbody>
					  	</Table>
					  </Accordion.Body>
					</Accordion.Item>
					</Accordion>
					</Col>
					</Row>
				)
			}));
		})

	}, [userOrders])

	return(
		!user.isAdmin || user !== null || user !== undefined
		?
		<Container fluid style={{marginTop: '6rem'}}>
			<Row className="mt-5">
				{userOrders}
			</Row>
		</Container>
		:
		<PageNotFound />
	)
}