import { Fragment, useContext } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import UserContext from '../UserContext'

export default function Home(){
	const {currentPage, setCurrentPage} = useContext(UserContext);

	setCurrentPage('home')
	return(
		<Fragment>
			<Banner />
			<Highlights />
		</Fragment>
	)
}