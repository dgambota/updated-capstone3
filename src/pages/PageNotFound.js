import {Link} from 'react-router-dom';
import {Fragment} from 'react'

export default function PageNotFound() {
	return (
		<Fragment>
			<h1>Page Not Found</h1>
			<p>Go back to the <Link to = '/'>homepage</Link>.</p>
		</Fragment>
	)
}