import {Fragment} from 'react'
import {Container, Row, Col, Table, Button, Card} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import Swal2 from 'sweetalert2'
import UserContext from '../UserContext'
import PageNotFound from '../pages/PageNotFound'
import UserProfileModal, {showProfileModal} from '../components/UserProfileModal'

export default function UserManagement(){

	const [accounts, setAccounts] = useState([]);
  	const {user} = useContext(UserContext);

	function getProfile(account){
		fetch(`${process.env.REACT_APP_API_URL}/users/${account}`,{
			method: 'POST',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			showProfileModal(data);
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users`,{
			method: 'POST',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			let count = 0;
			setAccounts(data.map(account => {
				count++;
				return(
					<tr key = {account._id}>
					  <td>{count}</td>
					  <td>{account.firstName}</td>
					  <td>{account.lastName}</td>
  			          <td>{account.email}</td>
  			          <td>{account.isAdmin.toString()}</td>
  			          <td><Button onClick={e => getProfile(account._id)}>Profile</Button></td>
					</tr>
				)
			}));
		})
	}, [accounts])

	return(
		user.isAdmin === true
		?
		<Fragment>
			<Container className="mt-5">
			<UserProfileModal />
			<Row>
				<Col xl={12} className="mt-5">
					<Card style={{minHeight: '40rem'}}>
					<Card style={{ border: "2px solid black"}}>
					<Row className="mx-auto">
						<h3 className="my-3">User Management</h3>
					</Row>
					</Card>
					<Table striped className="mt-3">
				      <thead>
				        <tr>
				          <th>#</th>
				          <th>Firstname</th>
				          <th>Lastname</th>
				          <th>Email</th>
				          <th>Admin</th>
				        </tr>
				      </thead>
				      <tbody>
			          	{accounts}
				      </tbody>
				    </Table>
				    </Card>
				</Col>
			</Row>
			</Container>
		</Fragment>
		:
		<PageNotFound />
	)
}