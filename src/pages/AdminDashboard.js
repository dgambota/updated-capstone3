/* IMPORT REACT MODULES */
import {Button, Container, Row, Col, Table, Dropdown, Form, Card} from 'react-bootstrap';
import {useContext, useState, useEffect} from 'react'
import {useParams, Link} from 'react-router-dom';

/* IMPORT OTHER REQUIRED MODULE */
import UserContext from '../UserContext';
import Swal2 from 'sweetalert2'

/* IMPORT MANUALLY DEFINED COMPONENTS */
import ToastNotification, {showToast} from '../components/ToastNotification'
import UpdateProductModal, {showModal} from '../components/UpdateProductModal'
import AddProductModal, {ShowAddModal} from '../components/AddProductModal'
import PageNotFound from '../pages/PageNotFound';

export default function AdminDashboard(){

	const [isSearching, setIsSearching] = useState(false);
	const [searchBox, setSearchBox] = useState('');
	const [sort, setSort] = useState('');
	const [updateProdDetails, setUpdateProdDetails] = useState([]);
	const {user} = useContext(UserContext);

	/* GET PRODUCT STATE-DASHBOARD STATE */
	const [products, setProducts] = useState([])
	

	/* FUNCTION TO UPDATE ISACTIVE VALUE - ACTIVATE PRODUCT */
	function activateProduct(id){
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/activate` , {
			method: 'PUT',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			if(data === true){
				showToast({
					title: 'Successfully Activated Product',
					description: 'Check your products',
					bgcolor: 'success'
				})
			}
		})
	}
	/* END ACTIVATE PRODUCT FUNCTION */

	/* FUNCTION TO UPDATE ISACTIVE VALUE - DEACTIVATE PRODUCT */
	function deactivateProduct(id){
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive` , {
			method: 'PUT',
			headers: {
				'Content-type' : 'application/json',
				'Authorization' : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			if(data === true){
				showToast({
					title: 'Deactivated Product',
					description: 'Check your products',
					bgcolor: 'danger'
				})
			}
		})
	}
	/* END DEACTIVATE PRODUCT FUNCTION */

	/* FUNCTION TO FILL DASHBOARD WITH UPDATED PRODUCTS DATA */
	useEffect(() => {
		let fetchDetails;
		if(isSearching){
			fetchDetails = {
				route: `/products/searchAll`,
				content: searchBox
			}
		} else {
			fetchDetails = {
				route: `/products/sort`,
				content: sort
			}
		}

		fetch(`${process.env.REACT_APP_API_URL}${fetchDetails.route}`,{
			method: 'POST',
			headers: {
				'Content-type' : 'application/json'
			},
			body: JSON.stringify({
				data: fetchDetails.content
			})
		})
		.then(response => response.json())
		.then(data => {
			let count = 0;
			setProducts(data.map(products => {
				count++;
				return(
					<tr key = {products._id}>
					  <td>{count}</td>
  			          <td>{products.name}</td>
  			          <td>{products.description}</td>
  			          <td>{products.price}</td>
  			          <td>{products.quantity}</td>
  			          <td>{products.isActive.toString()}</td>
  			          <td><Button onClick={e => editProduct(products._id)}>Update</Button></td>
			          {
			          	products.isActive === true 
      			          ?
      			          <td><Button variant='danger' onClick={e => deactivateProduct(products._id)}>Deactivate</Button></td>
      			          :
      			          <td><Button variant='success' onClick={e => activateProduct(products._id)}>Activate</Button></td>
      			      }
					</tr>
				)
			}));
		})
	}, [products])	
	/* END FUNCTION TO FILL DASHBOARD */

	/* FUNCTION TO PASS SELECTED PRODUCT DETAILS TO BE UPDATED AND SHOW MODAL TO EDIT*/
	function editProduct(prod){
		fetch(`${process.env.REACT_APP_API_URL}/products/${prod}`)
		.then(response => response.json())
		.then(data => {
		 	showModal(data)
		})
	}
	/* END FUNCTION EDIT PRODUCT DETAILS */


	useEffect(() => {
		searchBox != '' ? setIsSearching(true) : setIsSearching(false)
	}, [searchBox])


	return(
		user.isAdmin === true
		?

		<Container fluid>
		<UpdateProductModal />
		<AddProductModal />
		<Row className="mt-5">
			<Col xl={3} className="mt-5">
				<Row>
				<Col xl={3}>
				<Dropdown>
			      <Dropdown.Toggle variant="outline-success" id="dropdown-basic">
			        Sort
			      </Dropdown.Toggle>

			      <Dropdown.Menu>
			        <Dropdown.Item onClick={() => setSort('nameAscend')}>Ascending</Dropdown.Item>
			        <Dropdown.Item onClick={() => setSort('nameDescend')}>Descending</Dropdown.Item>
			        <Dropdown.Item onClick={() => setSort('')}>Reset</Dropdown.Item>
			      </Dropdown.Menu>
			    </Dropdown>
			    </Col>
			    <Col xl={6}>
			    <Button variant="outline-success" onClick={() => ShowAddModal()}>Add New Product</Button>
			    </Col>
			    </Row>
		    </Col>
		   
			<Col xl={5} className="mt-5">
				<Row>
					<Col xl={9}>
					<Form.Control type="text" placeholder="Search" value={searchBox} onChange={e => setSearchBox(e.target.value)}/>
					</Col>
				</Row>
			</Col>
			<Col xl={4} className="mt-5">
				<h5 className="mt-1">ADMIN DASHBOARD</h5>
			</Col>
		</Row>
		<Row className='mt-5'>
			<Col>
				<Card style={{minHeight: '40rem'}} style={{ border: "2px solid black"}}>
				<Table striped>
			      <thead>
			        <tr>
			          <th>#</th>
			          <th>Product Name</th>
			          <th>Description</th>
			          <th>Price</th>
			          <th>Stock</th>
			          <th>Active</th>
			          <th>Actions</th>
			        </tr>
			      </thead>
			      <tbody>
		          	{products}
			      </tbody>
			    </Table>
			    </Card>
			</Col>
		</Row>
		</Container>
		:
		<PageNotFound />
	)
}