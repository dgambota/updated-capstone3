/* REACT MODULES */
import { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'
import {BrowserRouter, Route, Routes} from 'react-router-dom'

/* MANUAL DEFINED COMPONENTS */
import AppNavBar from './components/AppNavBar'
import SingleProductView from './components/SingleProductView';
import LoadingPlaceholder from './components/LoadingPlaceholder'
import ToastNotification from './components/ToastNotification'
import UploadImageController from './components/UploadImageController'
import PaginationHandler from './components/Pagination'

/* MANUAL DEFINED PAGES */
import Products from './pages/Products';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound'
import AdminDashboard from './pages/AdminDashboard';
import UserManagement from './pages/UserManagement' 
import Cart from './pages/Cart'
import OrderHistory from './pages/OrderHistory';


/* OTHER REQUIRED IMPORT */
import {UserProvider} from './UserContext';
import './App.css';


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  const [currentPage, setCurrentPage] = useState('');
  const [currentPostsList, setCurrentPostsList] = useState([]);

  /* GET LOGGED IN USER DETAILS AND PASS TO USERCONTEXT TO USE GLOBALLY */
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(data => {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    })
  }, [])
  /* END USERCONTEXT VALUE PASSING */
  
  return (
    <UserProvider value = {{user, setUser, unsetUser, currentPage, setCurrentPage, currentPostsList, setCurrentPostsList}}>
      <BrowserRouter>
        <AppNavBar />
        <ToastNotification />
        <UploadImageController />
        <Container fluid>  
            <Routes>
              <Route path='/' element = {<Home/>} />
              <Route path='/adminDashboard' element = {<AdminDashboard/>} />
              <Route path='/users' element = {<UserManagement/>} />

              <Route path='/register' element = {<Register/>} />
              <Route path='/login' element = {<Login/>} />
              <Route path='/logout' element = {<Logout/>} />

              <Route path='/cart' element = {<Cart/>} />
              <Route path='/orders' element = {<OrderHistory />} />
              <Route path='/products' element = {<Products/>} />
              <Route path='/products/:id' element = {<SingleProductView/>} />
              <Route path="*" element={<PageNotFound />} />
              
            </Routes>
        </Container>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
